import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class StocksDataService {

  constructor(private http:HttpClient) { }

  getData(x:String)
  {
  
    let url="https://cloud.iexapis.com/stable/stock/"+x+"/chart/1m?token=pk_1931a5373de44783a1349c58bc38a69e&chartIEXOnly=True";
    
    return this.http.get(url);
    
  }
  
}
