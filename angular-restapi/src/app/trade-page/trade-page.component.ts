import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestapiService } from "../shared/restapi.service";
import{StocksDataService} from '../trade-page/stocks-data.service';

import { ChartOptions, ChartType} from 'chart.js';
import {Chart} from 'chart.js'
// import '../chartjs-chart-financial/chartjs-chart-financial';

@Component({
  selector: 'app-trade-page',
  templateUrl: './trade-page.component.html',
  styleUrls: ['./trade-page.component.css']
})
export class TradePageComponent implements OnInit {
  @Input() stocks = { id: 0, stockTicker: '', buySell: '', price: 0, volume: 0 ,statusCode:0,category:'',dtime: new Date(),company:''}
  currentVal="";
  public data:any = []
  chart = [];
  public res:any
  constructor(
    private stock:StocksDataService,
    public restApi: RestapiService,
    public router: Router
  ) { }

  ngOnInit() {
    
  }


  addStock() {
    console.log(this.stocks)
    
    this.restApi.createStock(this.stocks).subscribe((data: {}) => {
      this.router.navigate(['/stock-list'])
    })
  }

    getVal(val: string)
  {
    
    this.currentVal=val;
    
    this.stock.getData(this.currentVal).subscribe((res: any)=>
     {
      this.data=res;
    //  console.log(this.data)
    
    //  var outputArray = [];  
    // for (let element in res) {  

    //     outputArray.push({  
        
    //     low:  jsonObject[element]
    //   });  
    // }
    // console.log(outputArray)
    // var lineChartData: ChartDataSets[] = [
    //   { data: outputArray, label: 'Series A' },
    //   { data: [65, 49, 90, 71, 56, 55, 40], label: 'Series B' },
    // ];
    // let close: Array<number>=[];
    // for(let element in res){
    //   close.push(element["close"])
    // } 

  }  
  
  

  )}
  
  


  
//    <div class="chart">
//   <canvas baseChart width="400" height="400"
//     [datasets]="financialChartData"
//     [options]="financialChartOptions"
//     [colors]="financialChartColors"
//     [legend]="financialChartLegend"
//     [chartType]="financialChartType"
//     [plugins]="financialChartPlugins">
//   </canvas>
// </div> 
  // public financialChartData: any[] = [
  //   {
  //     label: 'Stock data of {{this.currentVal}}',
  //     data:  this.data
  //   },
  // ];
  // public financialChartOptions: ChartOptions = {
  //   responsive: true,
  //   maintainAspectRatio: false,
  // };
  // public financialChartColors = [
  //   {
  //     borderColor: 'black',
  //     backgroundColor: 'rgba(255,0,0,0.3)',
  //   },
  // ];
  // public financialChartLegend = true;
  // public financialChartType: ChartType = 'line';
  // public financialChartPlugins = [];

}



