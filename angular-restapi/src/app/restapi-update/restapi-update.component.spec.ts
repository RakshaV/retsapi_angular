import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestapiUpdateComponent } from './restapi-update.component';

describe('RestapiUpdateComponent', () => {
  let component: RestapiUpdateComponent;
  let fixture: ComponentFixture<RestapiUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestapiUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestapiUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
