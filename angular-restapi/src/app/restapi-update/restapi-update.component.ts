import { Component, OnInit } from '@angular/core';
import { RestapiService } from "../shared/restapi.service";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-restapi-update',
  templateUrl: './restapi-update.component.html',
  styleUrls: ['./restapi-update.component.css']
})
export class RestapiUpdateComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  stockDetails: any = {};
  constructor(
    public restApi: RestapiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.restApi.getstocksbyId(this.id).subscribe((data: {}) => {
      this.stockDetails = data;
    })
  }

  updateStocks() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateStocks(this.id, this.stockDetails).subscribe(data => {
        this.router.navigate(['/stock-list'])
      })
    }
  }

}
