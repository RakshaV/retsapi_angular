import { isNull } from "@angular/compiler/src/output/output_ast";

export class Stocks {
     constructor(){
    this.id = 0;
		this.stockTicker = "";
		this.buySell = "";
		this.price=0;
		this.statusCode=0;
    this.volume=0;
    this.category="";
    this.company="";
    this.dtime= new Date();
    }

    id : number;
    stockTicker:string;
    buySell:string;
    price: number;
    statusCode: number;
    volume: number;
    category:string;
    dtime: Date;
		company:string;
}
