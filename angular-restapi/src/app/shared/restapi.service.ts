import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stocks } from '../shared/stocks';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RestapiService {

  apiURL = 'http://localhost:8080/api/stocks/';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  // HttpClient API post() method 
  createStock(stocks:Stocks): Observable<Stocks> {
    console.log("im inside createstock")
    return this.http.post<Stocks>(this.apiURL + '', JSON.stringify(stocks), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 
  
  // HttpClient API get() method 
  getStocks(): Observable<Stocks> {
    return this.http.get<Stocks>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
    console.log('test')
  }

  // HttpClient API get() method 
  getstocksbyId(id:any): Observable<Stocks> {
    return this.http.get<Stocks>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API put() method
  updateStocks(id:number, Stocks:Stocks): Observable<Stocks> {
    return this.http.put<Stocks>(this.apiURL, JSON.stringify(Stocks), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method 
  deleteStocks(id:number){
    return this.http.delete<Stocks>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

   // Error handling 
   handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
