import { stringify } from '@angular/compiler/src/util';
import { Component,OnInit } from '@angular/core';
import{StocksDataService} from './trade-page/stocks-data.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
title = 'angular-restapi';
 currentVal="AMZN";
 public data:any = []
 public res:any
 constructor(private stock:StocksDataService)
 {
   this.stock.getData(this.currentVal).subscribe((res: any)=>
    {
      this.data=res
     
      
    })

  
 }

 getVal(val: string)
 {
   
   this.currentVal=val;
   console.warn(this.currentVal)
   
 }
 

}

