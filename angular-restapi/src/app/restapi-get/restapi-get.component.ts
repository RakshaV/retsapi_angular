import { Component, OnInit } from '@angular/core';
import { RestapiService } from "../shared/restapi.service";
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
@Component({
  selector: 'app-restapi-get',
  templateUrl: './restapi-get.component.html',
  styleUrls: ['./restapi-get.component.css']
})
export class RestapiGetComponent implements OnInit {
    Stocks : any = [];
    selectedCategory="";
    searchbyDate="";
    searchbyName="";
    searchText="";
  constructor(
    
    public restApi: RestapiService,
    
    
  ) { }

  ngOnInit(): void {
    console.log('test')
    this.loadStocks()
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
    // this.searchText = this.selectedCategory || this.searchbyDate || this.searchbyName ;
    // console.log(this.searchText);
  }
  loadStocks() {
    console.log('test');
    return this.restApi.getStocks().subscribe((data: {}) => {   
    
      this.Stocks = data;
       
    })
  }

  deleteStocks(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteStocks(id).subscribe(data => {
        this.loadStocks()
      })
    }
  } 
  
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      labels: { fontColor: 'black' }
    },
    
  };
  public pieChartLabels: Label[] = ['Pharma','Software','Businuess Services','Finance','Electrical'];
  public pieChartData: SingleDataSet = [10, 15,12,20,10];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: ['#01579B', '#039BE5','#29B6F6','#81D4FA','#B3E5F6'],
    },
  ];

}
