import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestapiGetComponent } from './restapi-get.component';

describe('RestapiGetComponent', () => {
  let component: RestapiGetComponent;
  let fixture: ComponentFixture<RestapiGetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestapiGetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestapiGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
