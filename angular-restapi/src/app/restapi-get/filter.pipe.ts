import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'appFilter' })
export class FilterPipe implements PipeTransform {
  dataCategory = [];
  transform(Stocks: any[], searchText: string): any[] {
      // console.log(Stocks);
    if (!Stocks || searchText=="--Clear All--") {
      return [];
    }
    if(!searchText ){
      return Stocks;
    }
    if ( searchText=="--List All--") {
      return Stocks;
    }
    // searchText = searchText.toLocaleLowerCase();
    // console.log(searchText);
    return Stocks.filter(stock => stock.category.indexOf(searchText) !== -1);
  }
}