import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filter' })
export class SearchNamePipe implements PipeTransform {
  dataCategory = [];
  transform(Stocks: any[], searchText: string): any[] {
      console.log(Stocks);
    if (!Stocks) {
      return [];
    }
    

    if (!searchText) {
      return [];
    }
    console.log(searchText);
    return Stocks.filter(stock => stock.company.indexOf(searchText) !== -1);
    // searchText = searchText.toLocaleLowerCase();
    
  }
}