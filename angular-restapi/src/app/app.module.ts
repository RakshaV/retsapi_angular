import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradePageComponent } from './trade-page/trade-page.component';
import { RestapiGetComponent } from './restapi-get/restapi-get.component';
import {FormsModule} from '@angular/forms';
import { RestapiUpdateComponent } from './restapi-update/restapi-update.component';
import { FilterPipe } from './restapi-get/filter.pipe'; 
import { AboutComponent } from './about/about.component';
import {SearchNamePipe} from './restapi-get/searchName.pipe'
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ChartsModule } from 'ng2-charts';
// import {SearchDatePipe} from './restapi-get/searchDate.pipe'

@NgModule({
  declarations: [
    AppComponent,
    RestapiGetComponent,
    RestapiUpdateComponent,
    TradePageComponent,
    FilterPipe,
    AboutComponent,
    SearchNamePipe
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng2SearchPipeModule,
    HttpClientModule,
    FormsModule,
    ChartsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
