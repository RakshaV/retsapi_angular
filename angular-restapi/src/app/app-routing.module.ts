import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { RestapiGetComponent } from './restapi-get/restapi-get.component';
import { RestapiUpdateComponent } from './restapi-update/restapi-update.component';
import { TradePageComponent } from './trade-page/trade-page.component';
import {AboutComponent} from './about/about.component'




const routes: Routes = [
  { path: '', redirectTo: 'about', pathMatch: 'full' },
  { path: 'trade', component: TradePageComponent },
  { path: 'stock-list', component: RestapiGetComponent },
  {path: 'stock-edit/:id', component: RestapiUpdateComponent},
  { path: 'home', component: AppComponent },
  { path: 'about', component: AboutComponent }

  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

